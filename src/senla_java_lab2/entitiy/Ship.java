package senla_java_lab2.entitiy;

import com.sun.xml.internal.bind.v2.model.core.ID;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Ship {

    public int ID;
    public ContainerOne One;
    public ContainerTwo Two;
    public Ship(int ID, ContainerOne One, ContainerTwo Two){
        this.ID=ID;
        this.One=One;
        this.Two=Two;
    }
}