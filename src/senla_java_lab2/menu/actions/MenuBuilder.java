package senla_java_lab2.menu.actions;
import senla_java_lab2.menu.actions.Menu;
import senla_java_lab2.menu.actions.actions.*;
import lombok.Getter;
public class MenuBuilder {

    @Getter
    public Menu mainMenu;

    private static MenuBuilder instance;

    private MenuBuilder() {
        buildMenu();
    }

    public static MenuBuilder getInstance() {
        if (instance == null) {
            instance = new MenuBuilder();
        }
        return instance;
    }

    public void buildMenu() {
        mainMenu = new Menu("Main");
        mainMenuInit();

        Navigator.getInstance().setCurrentMenu(mainMenu);
    }

    public void mainMenuInit() {
        mainMenu.getMenuItems().add(new MenuItem("1. Посмотреть сколько воды в порту", new DisplayPortInfoAction(), null, null));
        mainMenu.getMenuItems().add(new MenuItem("2. Посмотреть список кораблей в порту", new DisplayShipInPortAction(), null, null));
        mainMenu.getMenuItems().add(new MenuItem("3. Удалить корабль из порта", new DeleteShipPort(), null, null));
        mainMenu.getMenuItems().add(new MenuItem("4. Создать корабль", new AddShip(), null, null));
        mainMenu.getMenuItems().add(new MenuItem("5. Посмотреть список кораблей, ожидающих прибытия в порт", new DislayWaitingShip(), null, null));
        mainMenu.getMenuItems().add(new MenuItem("6. Загрузить корабль в порт", new MoveShipPort(), null, null));
        mainMenu.getMenuItems().add(new MenuItem("7. Выйти из программы", new ExitAction(), null, null));
    }

}
