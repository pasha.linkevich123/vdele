package senla_java_lab2.menu.actions;

import lombok.Getter;
import lombok.Setter;

import javax.naming.Name;
import java.util.ArrayList;
import java.util.List;
@Getter @Setter
public class Menu {

    private List<MenuItem> menuItems = new ArrayList<>();

    private String name;

 /*   public void setMenuItem(List<MenuItem> menuItems)
    {
        this.menuItems=menuItems;
    }

    public List<MenuItem> getMenuItems()
    {
        return menuItems;
    }*/

    /*public void setName(String name)
    {
        this.name=name;
    }

    public String getName()
    {
        return name;
    }*/


    public Menu(String name) {
        this.name = name;
    }

    public Menu(List<MenuItem> menuItems) {
        this.menuItems = menuItems;
    }

    public Menu(String name, List<MenuItem> menuItems) {
        this.name = name;
        this.menuItems = menuItems;

    }
}