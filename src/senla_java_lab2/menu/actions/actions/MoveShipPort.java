package senla_java_lab2.menu.actions.actions;

import senla_java_lab2.constructor.Context;
import senla_java_lab2.entitiy.Port;
import senla_java_lab2.entitiy.Ship;
import senla_java_lab2.entitiy.ContainerTwo;
import senla_java_lab2.entitiy.ContainerOne;
import senla_java_lab2.menu.actions.Action;
import senla_java_lab2.menu.actions.MenuController;



public class MoveShipPort implements Action {
    public void execute() {
        System.out.println("Выберите корабль который хотите загрузить в порт - ");
        for (int i = 0; i < Context.waitingShips.size(); i++) {
            System.out.println("ID: " + Context.waitingShips.get(i).ID);
        }
        int download = MenuController.in.nextInt();
        for (int i = 0; i < Context.waitingShips.size(); i++) {
            System.out.println(Context.waitingShips.get(i).ID+" ");
            if (download == Context.waitingShips.get(i).ID) {
                Context.port.Capacity += Context.waitingShips.get(i).One.ContainerBig * 1000 + Context.waitingShips.get(i).Two.ContainerSmall * 450;
                Context.port.getShips().add(new Ship(Context.waitingShips.get(i).ID, new ContainerOne(Context.waitingShips.get(i).One.ContainerBig = 0), new ContainerTwo(Context.waitingShips.get(download - 1).Two.ContainerSmall = 0)));


                Context.waitingShips.remove(download);
            }
        }
    }

}
