package senla_java_lab2.menu.actions.actions;

import senla_java_lab2.menu.actions.Action;
import senla_java_lab2.constructor.Context;

public class DisplayPortInfoAction implements Action {
    @Override
    public void execute() {
        System.out.println("Порт содержит " + Context.port.getCapacity() + " воды");
    }
}
