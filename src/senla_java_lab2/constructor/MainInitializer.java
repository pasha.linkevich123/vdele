package senla_java_lab2.constructor;

import java.util.ArrayList;
import java.util.List;

import senla_java_lab2.entitiy.Port;
import senla_java_lab2.entitiy.Ship;
import senla_java_lab2.menu.actions.MenuController;

public class MainInitializer {
    public static void init() {
        initContext();
        initMenu();
    }

    private static void initContext() {
       Context.port = new Port();
        Context.waitingShips = new ArrayList<>();
        generatePort();
        generateWaitingShip();
    }

    private static void initMenu()
    {
        MenuController.getInstance().run();
    }

    private static void generateWaitingShip()
    {
        List<Ship> waitingShips = new ArrayList<>();
    }

    private static void generatePort() {
        Port port = new Port();
        port.setCapacity(0);
    }
}
