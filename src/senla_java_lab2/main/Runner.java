package senla_java_lab2.main;

import java.io.IOException;
import senla_java_lab2.constructor.MainInitializer;
public class Runner {
    public static void main(String[] args) throws IOException {

        MainInitializer initializer = new MainInitializer();
        initializer.init();
    }

}
